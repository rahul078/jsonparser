﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetEstimates
{
    public class EstimateList
    {
        public List<Estimate> EList { get; set; }
        public EstimateList()
        {
            EList = new List<Estimate>();
        }
    }
}