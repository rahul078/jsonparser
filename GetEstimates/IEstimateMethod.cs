﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetEstimates
{
    public interface IEstimateMethod
    {
        Estimate GetEstimates(int buildYear);
    }
}
